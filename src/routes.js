import React from 'react';
import {Router, Scene, Stack} from 'react-native-router-flux';

import Main from './pages/Main';
import Details from './pages/Details';

const Routers = () => (
  <Router>
    <Stack key="root">
      <Scene key="main" component={Main} hideNavBar initial />
      <Scene key="details" component={Details} hideNavBar />
    </Stack>
  </Router>
);

export default Routers;
