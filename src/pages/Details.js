import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';

export default function Details({url}) {
  const [loading, setLoading] = useState(true);
  const [details, setDetails] = useState(true);
  useEffect(() => {
    detailsHeroes();
  }, []);

  function detailsHeroes() {
    axios
      .get(url)
      .then(response => {
        setDetails(response.data);
        setLoading(false);
      })
      .catch(error => {
        console.log('ERRO', error);
      });
  }
  function renderHeader() {
    return (
      <View style={styles.viewHeader}>
        <TouchableOpacity onPress={() => Actions.pop()}>
          <Icon name="arrow-left" color="#fff" size={26} />
        </TouchableOpacity>
        <Text style={styles.title}>Hero</Text>
      </View>
    );
  }

  function renderContent() {
    return (
      <View style={styles.viewContent}>
        <Image
          resizeMode="contain"
          source={{uri: details.sprites.front_default}}
          style={styles.img}
        />
        <Text>{details.name}</Text>
        <View style={styles.viewContentChip}>
          {details.types.map(({type, index}) => {
            return (
              <View key={index} style={styles.viewChip}>
                <Text style={{color: '#fff'}}>{type.name}</Text>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      {renderHeader()}
      {!loading ? (
        renderContent()
      ) : (
        <ActivityIndicator size="large" color="#D32026" />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFEBEE',
  },
  viewContent: {
    flex: 1,
    marginVertical: 40,
    marginHorizontal: 20,
    backgroundColor: '#fff',
    borderRadius: 4,
    alignItems: 'center',
  },
  viewHeader: {
    backgroundColor: '#D32026',
    height: 56,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  img: {
    height: 120,
    width: 120,
  },
  viewContentChip: {
    flexDirection: 'row',
    marginTop: 30,
  },
  viewChip: {
    backgroundColor: 'red',
    borderRadius: 20,
    height: 30,
    width: 60,
    marginHorizontal: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: '#fff',
    fontSize: 20,
    marginLeft: 110,
  },
});
