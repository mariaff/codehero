/* eslint-disable prettier/prettier */
import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  FlatList,
  TextInput,
  ActivityIndicator,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';

import {Actions} from 'react-native-router-flux';

export default function Main() {
  const [heroes, setHeroes] = useState();
  const [loading, setLoading] = useState(true);

  // useEffect(() => {
  //   const fetchHeroes = async () => {
  //     try {
  //       const response = await axios.get('https://developer.marvel.com', {
  //         headers: {
  //           apikey: '9658b045e0c07184b1e433e26ad9eb7b',
  //           limit: 4,
  //         },
  //         responseType: 'json',
  //       });
  //       const responseJson = await response.data.json();
  //       console.log('FOOOOI', responseJson);
  //       // setHeroes(data);
  //     } catch (error) {
  //       console.log('ERROU', error);
  //     }
  //   };
  //   fetchHeroes();
  // }, []);

  useEffect(() => {
    fetchHeroes();
  }, []);

  function fetchHeroes() {
    axios
      .get('https://pokeapi.co/api/v2/pokemon/')
      .then(response => {
        setHeroes(response.data.results);
        setLoading(false);
        // if(!loading) {
        //   detailsHeroes()
        // }
        // console.log('RESULT', response.data.results);
      })
      .catch(error => {
        console.log('ERRO', error);
      });
  }

  function handlerSearch(text) {
    let formatedText = text.toLowerCase();
    const filterData = heroes.filter(hero => {
      const itemData = `${hero.name.toLowerCase()}`;
      return itemData.indexOf(formatedText) > -1;
    });
    setHeroes(filterData);
  }

  function renderSearch() {
    return (
      <View style={{flexDirection: 'column'}}>
        <Text>Busca Marvel Teste Front-End</Text>
        <View style={{marginVertical: 12}}>
          <Text style={{color: '#D42026'}}>Nome do personagem</Text>
          <TextInput
            style={styles.textInputSearch}
            onChangeText={text => handlerSearch(text)}
          />
        </View>
      </View>
    );
  }

  function renderSeparator() {
    return <View style={styles.separator} />;
  }
  function renderList() {
    if (loading) {
      return (
        <View style={styles.activityIndicator}>
          <ActivityIndicator size="large" color="#D42026" />
        </View>
      );
    }
    return (
      <FlatList
        data={heroes}
        ItemSeparatorComponent={renderSeparator}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              key={item.url}
              onPress={() => Actions.push('details', {url: item.url})}>
              <View style={styles.contentList}>
                {/* <Image /> */}
                <View style={styles.viewItem} />
                <Text>{item.name}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
        ListFooterComponent={({item}) => {
          if (!loading) return null;
          return (
            <View style={{alignSelf: 'center', marginVertical: 20}}>
              <ActivityIndicator color="#D42026" />
            </View>
          );
        }}
      />
    );
  }

  function renderPaginator() {
    return (
      <View style={styles.viewPaginator}>
        <Icon name="chevron-left" size={40} />
        <View style={{flexDirection: 'row'}}>
          <View style={styles.viewPage}>
            <Text>1</Text>
          </View>
          <View style={styles.viewPage}>
            <Text>2</Text>
          </View>
          <View style={styles.viewPage}>
            <Text>3</Text>
          </View>
        </View>
        <Icon name="chevron-right" size={40} />
      </View>
    );
  }

  return (
    <View style={styles.viewContainer}>
      <View style={{paddingHorizontal: 20}}>{renderSearch()}</View>
      <View style={styles.viewHeadTable}>
        <Text style={{color: '#fff', marginLeft: 85}}>Nome</Text>
      </View>
      <View style={{flex: 8}}>{renderList()}</View>
      <View style={styles.viewFooter}>{renderPaginator()}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  viewHeadTable: {
    backgroundColor: '#D42026',
    height: 30,
    justifyContent: 'center',
  },
  activityIndicator: {
    flex: 1,
    backgroundColor: '#fff',
  },
  textInputSearch: {
    height: 40,
    borderWidth: 1,
    borderColor: '#616161',
    borderRadius: 2,
  },
  primaryColor: {
    color: '#D42026',
  },
  separator: {
    flex: 1,
    height: 1,
    backgroundColor: '#D42026',
  },
  viewFooter: {
    backgroundColor: '#fff',
    flex: 1,
    paddingHorizontal: 30,
  },
  viewPaginator: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewPage: {
    backgroundColor: '#D42026',
    height: 20,
    width: 20,
    borderRadius: 10,
    alignItems: 'center',
    marginHorizontal: 8,
  },
  contentList: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  viewItem: {
    marginVertical: 18,
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: 'red',
    marginRight: 20,
  },
});
